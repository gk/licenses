# Licenses...

This repo exists so far to store the network-health licenses in a versionable
format, to solve
https://gitlab.torproject.org/tpo/network-health/team/-/issues/275, which is
part of https://gitlab.torproject.org/tpo/team/-/issues/114.

[nethealth licenses](nethealth_licenses.md)

[nethealth tools list](https://gitlab.torproject.org/tpo/network-health/team/-/wikis/Network-health-tools)

[tools to check licenses](tools_licenses.md)
